import os, os.path as osp
from   distutils.spawn import find_executable
import argparse
import logging
import traceback
import sqlite3
import time

import csv

import requests
import zillow

import emails

import dataset

from selenium                       import webdriver
from selenium.common.exceptions     import TimeoutException, NoSuchElementException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by   import By
from selenium.webdriver.support.ui  import WebDriverWait
from selenium.webdriver.support     import expected_conditions as ec

from pyvirtualdisplay import Display

display = Display(visible = 0, size = (800, 800))
display.start()

log = logging.getLogger(__name__)
log.setLevel(logging.NOTSET)

def which(executable, raise_err = False):
    exec_ = find_executable(executable)
    if not exec_:
        if raise_err:
            raise ValueError('{executable} executable not found.'.format(
                executable = executable
            ))

    return exec_

def get_digit_int(string):
    return float(''.join([i for i in string if i.isdigit() or i == '.']))

class Realtor:
    BASEURL = "https://www.realtor.com"

    def search(self, query, slug, threshold = 30):
        driver  = webdriver.Chrome(which('chromedriver', raise_err = True))
        results = [ ]

        try:
            # Open realtor.com
            driver.get(Realtor.BASEURL)
            print("Current URL: {url}".format(url = driver.current_url))

            # Input the SearchBox with Details.
            print('Inputting query "{query}" into Search Box.'.format(
                query = query
            ))
            e = driver.find_element_by_id('searchBox')
            e.send_keys(query)
            print("Dispatching Key: ENTER")
            e.send_keys(Keys.RETURN)

            try:
                print("Waiting for {time} seconds...".format(time = threshold))
                WebDriverWait(driver, threshold).until(
                    ec.presence_of_element_located(
                        (By.ID, 'desktop-facet-price')
                    )
                )

                print("Current URL: {url}".format(url = driver.current_url))
                if not "realestateandhomes-search" in driver.current_url:
                    print("WARNING: Unable to fetch Search Results. Try changing the threshold maybe?")
                else:
                    url = osp.join(driver.current_url, slug)

                    print("Fetching URL: {url}".format(url = url))
                    driver.get(url)

                    print("Current  URL: {url}".format(url = driver.current_url))

                    pages = 1
                    try:
                        e = WebDriverWait(driver, threshold).until(
                            ec.presence_of_element_located(
                                (By.CLASS_NAME, 'pagination')
                            )
                        )
                        print("Pagination Found!")
                        e = WebDriverWait(driver, threshold).until(
                            ec.presence_of_all_elements_located(
                                (By.XPATH, "//a[starts-with(@data-omtag, 'srp:paging')]")
                            )
                        )
                        e = e[-2]

                        pages = int(e.text)

                        print("{pages} pages found.".format(
                            pages = pages
                        ))
                    except TimeoutException:
                        traceback.print_exc()
                        print("No Pagination found.")

                    BASEURL  = driver.current_url

                    for i in range(1, pages + 1):
                        url = osp.join(BASEURL, 'pg-{page}'.format(page = i))
                        driver.get(url)

                        card_view = False

                        try:
                            elements = WebDriverWait(driver, threshold).until(
                                ec.presence_of_all_elements_located(
                                    (By.CLASS_NAME, 'srp-item')
                                )
                            )
                        except Exception:
                            print("Card View...")
                            card_view = True
                            elements = WebDriverWait(driver, threshold).until(
                                ec.presence_of_all_elements_located(
                                    (By.CLASS_NAME, 'component_property-card')
                                )
                            )

                        print("Found {nres} results for Page {page}.".format(
                            page = i,
                            nres = len(elements)
                        ))

                        for e in elements:
                            result = { }

                            result['url']     = Realtor.BASEURL + e.get_attribute('data-url')
                            
                            
                            _e = e.find_element_by_class_name('data-price' if card_view else 'data-price-display')
                            result['price']   = get_digit_int(_e.text.strip())

                            _e = e.find_element_by_class_name('listing-street-address')
                            result['address'] = _e.text.strip()

                            _e = e.find_element_by_class_name('listing-city')
                            result['city']    = _e.text.strip()

                            _e = e.find_element_by_class_name('listing-region')
                            result['state']   = _e.text.strip()

                            _e = e.find_element_by_class_name('listing-postal')
                            result['postal']  = _e.text.strip()

                            print("Parsed Result: {result}".format(result = result))

                            results.append(result)

                        for result in results:
                            print("Fetching URL: {url}.".format(url = result['url']))
                            driver.get(result['url'])

                            print("Searching for Days on Realtor...")
                            _e = WebDriverWait(driver, threshold).until(
                                ec.presence_of_element_located(
                                    (By.CLASS_NAME, 'ra-days-on-realtor')
                                )
                            ).find_element_by_xpath('..').find_element_by_class_name('key-fact-data')
                            result['since']   = _e.text

                            print("Fetching Property Type...")
                            _e = WebDriverWait(driver, threshold).until(
                                ec.presence_of_element_located(
                                    (By.CLASS_NAME, 'ra-property-type')
                                )
                            ).find_element_by_xpath('..').find_element_by_class_name('key-fact-data')
                            result['type']    = _e.text

            except TimeoutException:
                traceback.print_exc()
                print("ERROR: Timeout! Looks like the page took a long time to load \
                    or there's no results to fetch.")
        except Exception as e:
            traceback.print_exc()
        finally:
            driver.close()

        return results

class Zillow:
    BASEURL = "https://www.zillow.com/find-your-home"

    def get_zestimate(self, query, threshold = 30):
        driver = webdriver.Chrome(which('chromedriver', raise_err = True))

        try:
            # Open realtor.com
            driver.get(Zillow.BASEURL)

            e = WebDriverWait(driver, threshold).until(
                ec.presence_of_element_located(
                    (By.CLASS_NAME, 'search-input')
                )
            )
            e.send_keys(query)
            e.send_keys(Keys.RETURN)

            print("Waiting for {time} seconds...".format(time = threshold))
            elements = WebDriverWait(driver, threshold).until(
                ec.presence_of_all_elements_located(
                    (By.CLASS_NAME, 'photo-cards')
                )
            )

            for e in elements:
                _e = e.find_elements_by_class_name('zsg-photo-card-price')
                if _e:
                    result = _e[0].text.strip()
                    result = get_digit_int(result)
                    return result
        except Exception as e:
            traceback.print_exc()
        finally:
            driver.close()

        return None

MUNCIPALITIES = dict({
         'grand rapids': ['City of Kentwood', 'City of Grand Rapids'],
              'lansing': ['City of Lansing', 'Lansing Township'],
                  'ada': ['Ada Township'],
    'east grand rapids': ['City of Kentwood', 'City of Grand Rapids'],
         'east lansing': ['City of East Lansing', 'Meridian Charter Township']
})

def get_townships(query):

    for key, value in MUNCIPALITIES.items():
        if query.startswith(key):
            return value

    return None

class AccessMyGov:
    BASEURL = "https://accessmygov.com/Home/WelcomePage"

    def get_result(self, query, address, threshold = 30):
        url    = AccessMyGov.BASEURL
        result = dict()

        driver = webdriver.Chrome(which('chromedriver', raise_err = True))
        driver.set_page_load_timeout(60)

        print("Accessing URL: {url}.".format(url = url))
        try:
            townships = get_townships(query)
            for township in townships:
                driver.get(url)

                e = WebDriverWait(driver, threshold).until(
                    ec.presence_of_element_located(
                        (By.XPATH, "//a[contains(@href, '/MunicipalDirectory')]")
                    )
                )
                e.click()

                print("Fetching Township for Query {query}.".format(query = query))
                
                print("Checking for Township {township}.".format(
                    township = township
                ))
                e = WebDriverWait(driver, threshold).until(
                    ec.presence_of_element_located(
                        (By.XPATH, "//div[starts-with(text(), '{township}')]".format(
                            township = township
                        ))
                    )
                )
                e.click()

                e = WebDriverWait(driver, threshold).until(
                    ec.presence_of_element_located(
                        (By.ID, "SearchText")
                    )
                )
                e.clear()
                e.send_keys(address)
                e.send_keys(Keys.RETURN)

                try:
                    elements = WebDriverWait(driver, threshold).until(
                        ec.presence_of_all_elements_located(
                            (By.CLASS_NAME, "site-search-row")
                        )
                    )

                    counter = len(elements) - 1
                    for i in range(len(elements)):
                        elements = WebDriverWait(driver, threshold).until(
                            ec.presence_of_all_elements_located(
                                (By.CLASS_NAME, "site-search-row")
                            )
                        )
                        
                        elements[counter].click()

                        WebDriverWait(driver, 3 * threshold).until(
                            ec.invisibility_of_element_located(
                                (By.ID, "global-progress-window-overlay")
                            )
                        )

                        e = WebDriverWait(driver, threshold).until(
                            ec.presence_of_element_located(
                                (By.CLASS_NAME, "sresult-header1")
                            )
                        ) \
                        .find_elements_by_tag_name('span')[-1]

                        print("Realtor Address: {address}.".format(address = address))
                        print("AccessMyGov Address: {address}.".format(address = e.text))
                        
                        try:
                            try:
                                date, price \
                                = WebDriverWait(driver, threshold).until(
                                    ec.presence_of_element_located(
                                        (By.XPATH, '//div[contains(text(), "Sale History")]')
                                    )
                                )                                       \
                                .find_element_by_xpath('..')            \
                                .find_element_by_xpath('..')            \
                                .find_element_by_xpath('..')            \
                                .find_element_by_tag_name('tbody')      \
                                .find_elements_by_tag_name('tr')[0]     \
                                .find_elements_by_tag_name('td')[:2]

                                result['date']  = date.text
                                result['price'] = get_digit_int(price.text)
                            except Exception:
                                traceback.print_exc()
                                print("Looks like this property has no Sale History.")

                            _e = driver.find_element_by_xpath('//a[contains(text(), "Tax Information")]')
                            _e.click()

                            WebDriverWait(driver, 3 * threshold).until(
                                ec.invisibility_of_element_located(
                                    (By.ID, 'global-progress-window-overlay')
                                )
                            )

                            rows \
                            = WebDriverWait(driver, 3 * threshold).until(
                                ec.presence_of_all_elements_located(
                                    (By.XPATH, '//tr[contains(@name, "TaxHistory")]')
                                )
                            )[:2]

                            result['tax'] = \
                            (
                                get_digit_int(rows[0].find_elements_by_tag_name('td')[3].text) + \
                                get_digit_int(rows[1].find_elements_by_tag_name('td')[3].text)
                            ) / 12

                            return result
                        except Exception:
                            traceback.print_exc()
                            counter -= 1
                            driver.execute_script("window.history.go(-1)")

                except TimeoutException:
                    pass

        except Exception:
            traceback.print_exc()
        finally:
            driver.close()

        return result

class CrimeMapping:
    BASEURL = 'https://www.crimemapping.com/map/agency/197' # NOTE: Only for Michigan.
        
    def search(self, query, threshold = 30):
        score  = 0
        driver = webdriver.Chrome(which('chromedriver', raise_err = True))

        try:
            driver.get(CrimeMapping.BASEURL)

            print("Fetching CrimeMapping Results for query: {query}.".format(
                query = query
            ))

            WebDriverWait(driver, threshold).until(
                ec.invisibility_of_element_located(
                    (By.ID, 'modalBgTransparency')
                )
            )

            print("Waiting for {time} seconds...".format(time = threshold))
            e = WebDriverWait(driver, threshold).until(
                ec.element_to_be_clickable(
                    (By.CLASS_NAME, 'viewWhen')
                )
            )
            e.click()

            print("Waiting for {time} seconds...".format(time = threshold))
            e = WebDriverWait(driver, threshold).until(
                ec.element_to_be_clickable(
                    (By.XPATH, "//a[contains(@class, 'btnWhiteText') \
                        and text()='Previous 4 Weeks']")
                )
            )
            e.click()

            print('Inputting Query "{query}" into Search Box.'.format(
                query = query
            ))
            e = driver.find_element_by_id('locationSearch')
            e.send_keys(query)
            print("Submitting Query...")
            e = driver.find_element_by_class_name('locationgo')
            e.click()

            print("Sleeing for 5 seconds...")
            time.sleep(5)

            e = driver.find_element_by_class_name("recordCount")
            score = get_digit_int(e.text)

        except Exception as e:
            traceback.print_exc()
        finally:
            driver.close()

        return score

def get_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument('filename', help = 'Path/Filename')
    parser.add_argument('--zwsid',
        help     = 'ZWSID for Zillow',
        required = True
    )
    parser.add_argument('-s', '--slug',
        help     = "Slug to be passed to realtor.com's search results",
        required = True
    )
    parser.add_argument('-d', '--db',
        default = 'db.db',
        help    = 'Path to DataBase'
    )
    parser.add_argument('--email-to',
        default = 'achillesrasquinha@gmail.com',
        help    = 'Email to'
    )
    parser.add_argument('--email-subject',
        default = 'Realtor Report',
        help    = 'Email subject'
    )
    parser.add_argument('-t', '--threshold',
        type    = int,
        default = 30,
        help    = 'Increase waiting threshold for Selenium'
    )
    parser.add_argument('-V', '--verbose',
        action  = 'store_true',
        help    = 'Display Verbose Output'
    )

    return parser

def main():
    parser = get_parser()
    args   = parser.parse_args()

    path   = osp.abspath(args.filename)
    if not osp.exists(path):
        raise ValueError('{filename} filename does not exist.')
    else:
        if args.verbose:
            log.setLevel(logging.DEBUG)

        print('Opening File {path}.'.format(path = path))

        RESULTS = [ ]
        with open(path, 'r') as f:
            lines   = f.readlines()
            realtor = Realtor()

            for query in lines:
                query   = query.strip()
                print("Executing Query: {query}".format(query = query))

                results = realtor.search(query,
                    threshold = args.threshold,
                    slug      = args.slug
                )

                if results:
                    zill     = Zillow()
                    access   = AccessMyGov()
                    crimemap = CrimeMapping()
                
                print("Fetched {nres} results.".format(nres = len(results)))
                for result in results:
                    print("Result Recieved: {result}.".format(result = result))
                    if 'type' in result and 'Mobile' not in result['type']:
                        result['query'] = query
                        address         = ' '.join([
                            result['address'], result['city'],
                            result['state'],   result['postal']
                        ])

                        result['id']    = address

                        if result['id'] not in [r['id'] for r in RESULTS if 'id' in r]:
                            zapi     = zillow.ValuationApi()
                            print("Fetching Search Results from zillow.com...")

                            try:
                                place = zapi.GetSearchResults(args.zwsid,
                                    result['address'], result['postal']
                                )
                            
                                zest  = dict({
                                    key: value
                                        for key, value in place.zestimate.__dict__.items()
                                            if not key.startswith('__') and not callable(key)
                                })

                                print("Fetched  Zestimate for ZPID {zpid}: {zestimate}.".format(
                                    zpid = place.zpid, zestimate = zest
                                ))

                                result['zestimate'] = zest['amount']

                            except Exception as e:
                                print("ERROR: Unable to fetch results from zillow.com for query: {query}.".format(
                                    query = result
                                ))
                                traceback.print_exc()

                                zest = zill.get_zestimate(address, threshold = args.threshold)
                                print("Zestimate Recieved: {zestimate}".format(
                                    zestimate = zest
                                ))

                                result['zestimate'] = zest

                            input_   = ' '.join(result['address'].split(' ')[:2])
                            response = access.get_result(query, input_, threshold = args.threshold)

                            print("Response from accessmygov.com recieved: {response}.".format(
                                response = response
                            ))

                            result['last_sale_date']  = response['date']  if 'date'  in response else None
                            result['last_sale_price'] = response['price'] if 'price' in response else None
                            result['tax']             = response['tax']   if 'tax'   in response else None
                            
                            score    = crimemap.search(address, threshold = args.threshold)
                            print("Crime Score Recieved: {score}.".format(score = score))
                            result['crime'] = score

                            RESULTS.append(result)

        with open('realtor-report.csv', 'w') as f:
            writer = csv.writer(f, delimiter=',')
            
            writer.writerow(['City', 'Asking Price', 'Last Sold Price', 'Sold On', \
                'Zestimate', 'Equity', 'Listed Since', 'Crime', 'Taxes', 'Link'])
            
            for result in RESULTS:
                writer.writerow([
                    result['query'], result['price'], result['last_sale_price'], result['last_sale_date'],
                    result['zestimate'], result['zestimate'] - (result['price'] + 30000),
                    result['since'], result['crime'], result['tax'], result['url']
                ])
        
        message = emails.html(
            mail_from = ('Achilles Rasquinha', 'foobar@example.com'),
            subject   = 'Realtor Report',
            html      = "Hi there! Here's your Daily Realtor Report."
        )
        message.attach(
            data      = open('realtor-report.csv', 'rb'),
            filename = 'realtor-report.csv'
        )
        r = message.send(to = 'achillesrasquinha@gmail.com')
        r.raise_if_needed()
        assert r.status_code == 250

if __name__ == '__main__':
    main()