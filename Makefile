sample: install
	python script.py \
		--slug "beds-3/baths-1/price-1-75000/sqft-750/age-0+75?pgsz=50" \
		--zwsid "X1-ZWz18nn2fq1nuz_7lwvq" \
		--verbose \
		city.txt

install: clean
	pip install -r requirements.txt

clean:
	find . | grep -E "__pycache__" | xargs rm -rf
	
	clear



